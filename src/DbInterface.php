<?php
/**
* Library for work with databases
* 
* @version 1.8.2
* @copyright Copyright (C) 2004 - 2018 Richter
* @author Richter (richter@wpdom.com)
* @link http://wpdom.com
*/

namespace Ardzo\DbInterface;

use Ardzo\ErrorProcessor\ErrorProcessor;

class DbInterface
{
    public $DB;                                        // Work database (name for MySQL, filename for SQLite)
    public $DB_host;                                   // Database host
    public $DB_user;                                   // Database user
    public $DB_psw;                                    // Database password
    public $SQL;                                       // Current SQL query
    public $DB_type = 'MySQL';                         // Type of database (MySQL, SQLite)
    public $DB_err0 = 'Unable to connect to database'; // Code of error for "connect()"
    public $DB_err1 = 'Incorrect request to database'; // Code of error for "query()"
    public $link_id;                                   // Link identifier for MySQLi and SQLite functions
    public $log_queries = FALSE;                       // Log all queries to a file
    public $log_queries_filename;                      // Filename for logging of a queries

    function __construct()
    {
        $this->err_processor = new ErrorProcessor;
    }

    /** Get number of affected rows in previous operation */
    public function affectedRows()
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_affected_rows($this->link_id);
                break;
            case 'SQLite':
                return $this->link_id->changes();
                break;
        }
    }

    /** Close a connection to database */
    public function close()
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_close($this->link_id);
                break;
            case 'SQLite':
                $this->link_id->close();
                break;
        }
    }

    /** Open a connection to database */
    public function connect($err_actions = 'lmws')
    {
        switch ($this->DB_type) {
            case 'MySQL':
                if (!$link_id = @mysqli_connect($this->DB_host, $this->DB_user, $this->DB_psw))
                    $this->err_processor->processError($this->DB_err0, $err_actions);
                else
                    return $this->link_id = $link_id;
                break;
            case 'SQLite':
                if (!file_exists($this->DB))
                    $this->err_processor->processError($this->DB_err0, $err_actions);
                else
                    return $this->link_id = new \SQLite3($this->DB);
                break;
        }
    }

    /** Move internal result pointer */
    public function dataSeek($R, $row_number)
    {
        switch ($this->DB_type) {
             case 'MySQL':
                 return @mysqli_data_seek($R, $row_number);
                 break;
             case 'SQLite':
                 $R->reset();
                 for ($I=0; $I<$row_number; $I++) // Moving to a specified row
                     $R->fetchArray(SQLITE3_NUM);
                 break;
        }
    }

    /** Fetch a result row as an array */
    public function fetchArray($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_fetch_array($R);
                break;
            case 'SQLite':
                return $R->fetchArray(SQLITE3_BOTH);
                break;
        }
    }

    /** Fetch a result row as an associative array */
    public function fetchAssoc($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_fetch_assoc($R);
                break;
            case 'SQLite':
                return $R->fetchArray(SQLITE3_ASSOC);
                break;
        }
    }

    /** Fetch a result row as an object */
    public function fetchObject($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_fetch_object($R);
                break;
            case 'SQLite':
                $tmp = $R->fetchArray(SQLITE3_ASSOC);
                return (object)$tmp;
                break;
        }
    }

    /** Get a result row as an enumerated array */
    public function fetchRow($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_fetch_row($R);
                break;
            case 'SQLite':
                return $R->fetchArray(SQLITE3_NUM);
                break;
        }
    }

    /** Free result memory */
    public function freeResult($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_free_result($R);
                break;
            case 'SQLite':
                return $R->finalize();
                break;
        }
    }

    /** Get the ID generated from the previous INSERT operation
    * If AUTO_INCREMENT column exists only
    */
    public function insertId()
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_insert_id($this->link_id);
                break;
            case 'SQLite':
                return $this->link_id->lastInsertRowID();
                break;
        }
    }

    /** Get number of rows in result */
    public function numRows($R)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return @mysqli_num_rows($R);
                break;
            case 'SQLite':
                $I = 0;
                $R->reset();
                while($data = $R->fetchArray(SQLITE3_NUM)) {
                    $I++;
                }
                return $I;
                break;
        }
    }

    /** Sends a query to database, check result and processes an error if needed */
    public function query($query = '', $err_actions = 'lm')
    {
        $res = FALSE;
        if (!empty($query)) $this->SQL = $query;
        $time_begin = microtime();

        switch ($this->DB_type) {
            case 'MySQL':
                if (!$R = @mysqli_query($this->link_id, $this->SQL)) {
                    $this->err_processor->processError($this->DB_err1.' ('.$this->SQL.')', $err_actions);
                    $res = FALSE;
                } else
                    $res = $R;
                break;
            case 'SQLite':
                $R = $this->link_id->query($this->SQL);
                $res = $R;
                break;
        }

        $time_end = microtime();
        if ($this->log_queries && $this->log_queries_filename!='')
            error_log(date('r')."\t".$time_begin."\t".$time_end."\t".($res?1:0)."\t".str_replace(array("\n", "\r"), ' ', $this->SQL)."\n", 3, $this->log_queries_filename);

        return $res;
    }

    /** Get result data */
    public function result($R, $row, $field = 0)
    {
        switch ($this->DB_type) {
            case 'MySQL':
                $I = 0;
                $this->dataSeek($R, $row);
                $data = mysqli_fetch_array($R, MYSQLI_BOTH);
                return $data[$field];
                break;
            case 'SQLite':
                $I = 0;
                $this->dataSeek($R, $row);
                $data = $this->fetchAssoc($R);
                return $data[$field];
                break;
        }
    }

    /**
    * Select a database
    * For MySQL only
    */
    public function selectDb($database = '')
    {
        switch ($this->DB_type) {
            case 'MySQL':
                return mysqli_select_db($this->link_id, $database?$database:$this->DB);
                break;
            case 'SQLite':
                // Nothing to do with SQLite, "connect()" is enough
                break;
        }
    }
}
?>
