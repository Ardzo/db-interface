# Library for work with databases

## Introduction
This is a class on PHP for work with databases.  
Supported databases - MySQL, SQLite 3.  
Used driver for MySQL - MySQLi.  

## Requirements
* PHP 5.0+
* MySQL
* Windows or Unix
* class "Error_Processor"
* class "MailSender"
* library "Often Used Functions" (http://ardzo.com/ru/soft_various.php)

## Using
Copy "DbInterface.php" from `"src"` and required libraries onto server
and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.
In this folder there is PHP script, sample SQLite3 database and dump of MySQL database.
You can use it for fast testing of library.
