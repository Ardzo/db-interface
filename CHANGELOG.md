# Version history:

## **1.8.2** 2018-11-26
* Format of date at logging is changed

## **1.8.1** 2018-11-01
* Logging in function `"query()"` is improved

## **1.8** 2018-10-14
* Support of SQLite 3 is added
* All functions is renamed
* Function `"affectedRows()"` now return a value

## **1.7.1** 2018-10-13
* Class is renamed
* Namespace is added
* Work with logs is changed
* Ability to log of queries is improved

## **1.7** 2017-10-21
* Adapted for work with MySQLi driver
* Function `"DB_list_tables()"` is removed

## **1.6** 2010-10-03
* Functions `"DB_select_db()"` is added
* Function `"DB_db_query()"` is replaced by `"DB_query()"`
* Function `"DB_query()"` can write query to a log

## **1.5** 2007-03-31
* Function `"DB_list_tables()"` is added

## **1.4** 2004-07-24
* Adapted for class Error_Processor V2.0

## **1.3** 2004-04-27
* Function `"DB_connect"` now also writes error

## **1.2** 2004-04-22
* Function `"DB_affected_rows"` is added

## **1.1** 2004-04-06
* Function `"DB_result"` now requires only two parameters

## **1.0** 2004-04-01
* This is first release
