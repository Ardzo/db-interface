<?php
require 'lib_ouf.php';
require 'MailSender.php';
require 'ErrorProcessor.php';
require 'DbInterface.php';

use Ardzo\DbInterface\DbInterface;

$db = new DbInterface;
$db->err_processor->EP_log_fullname = 'db_errors.log';

$db->DB      = 't1';
//$db->DB      = 't1.db';
$db->DB_host = '127.0.0.1';
$db->DB_user = 'root';
$db->DB_psw  = 'root';
$db->DB_type = 'MySQL';
//$db->DB_type = 'SQLite';

$db->connect('lw');
$db->selectDb();

$db->SQL = "SELECT * FROM table_1";
$R = $db->query('', 'w');

$record = $db->fetchArray($R);
print_r($record);
$db->dataSeek($R, 0);
$record = $db->fetchAssoc($R);
print_r($record);
$record = $db->fetchObject($R);
print_r($record);
$record = $db->fetchRow($R);
print_r($record);

for ($I=0; $I<$db->numRows($R); $I++) {
    echo '<p />'.$db->result($R, $I, 'title');
}

$db->close();
?>
